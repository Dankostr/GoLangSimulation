package service

import (
	"fmt"
	"serviceworker"
	"structs"
	"variables"
)

func Service(HelpDesk chan structs.HelpDesk, GetMachineForRepair chan structs.MachineRepair) {
	statesSumMachines := make([]bool, variables.SumMachines)
	statesMultiplyMachines := make([]bool, variables.MultiplyMachines)
	serviceWorkers := make([]chan structs.ServiceInfo, 0)
	respondChannel := make(chan chan chan int)
	serviceWorkerChannel := make(chan structs.HelpDesk)
	tasks := make([]structs.ServiceTask, 0)
	getTask := make(chan chan structs.ServiceInfo)
	for i := 0; i < variables.Fixers; i++ {
		chForSw := make(chan structs.ServiceInfo)
		serviceWorkers = append(serviceWorkers, chForSw)
		go serviceworker.ServiceWorker(getTask)
	}
	for i := 0; i < variables.SumMachines; i++ {
		statesSumMachines[i] = true
	}
	for i := 0; i < variables.MultiplyMachines; i++ {
		statesMultiplyMachines[i] = true
	}
	for {
		select {
		case machine := <-HelpDesk:
			if (machine.Type == '+' && statesSumMachines[machine.Id] == false) || (machine.Type == '*' && statesMultiplyMachines[machine.Id] == false) {
				if variables.Issilent == false {
					fmt.Printf("HelpDesk: Ignoring report of machine id: %d of type:%c . Machine already under repairment \n", machine.Id, machine.Type)
				}
			} else {
				if machine.Type == '+' {
					statesSumMachines[machine.Id] = false
				} else if machine.Type == '*' {
					statesMultiplyMachines[machine.Id] = false
				}
				if variables.Issilent == false {
					fmt.Printf("HelpDesk: Accepting report of machine id: %d of type:%c . Machine is going for maintenance \n", machine.Id, machine.Type)
				}
				GetMachineForRepair <- structs.MachineRepair{machine, respondChannel}
				machineChannelForRepair := <-respondChannel
				tasks = append(tasks, structs.ServiceTask{structs.HelpDesk{machine.Id, machine.Type}, machineChannelForRepair})

			}
		case machine := <-serviceWorkerChannel:
			if machine.Type == '+' {
				statesSumMachines[machine.Id] = true
			} else if machine.Type == '*' {
				statesMultiplyMachines[machine.Id] = true
			}
			if machine.Type == '+' && variables.Issilent == false {
				fmt.Printf("Repaired summing machine id:%d\n", machine.Id)

			} else if machine.Type == '*' && variables.Issilent == false {
				fmt.Printf("Repaired multiplying machine id:%d\n", machine.Id)
			}
		case getServiceTask := <-maybe(len(tasks) > 0, getTask):
			v := tasks[0]
			tasks = tasks[1:]
			getServiceTask <- structs.ServiceInfo{v.Info, v.Channel, serviceWorkerChannel}
		}
	}
}

func maybe(b bool, c chan chan structs.ServiceInfo) chan chan structs.ServiceInfo {
	if !b {
		return nil
	}
	return c
}
