package worker

import (
	"fmt"
	"math/rand"
	"menu"
	"structs"
	"time"
	"variables"
)

func Worker(number int, read chan *structs.ReadOp, write chan *structs.WriteMagazine, getMachine chan *structs.GetMachine, HelpDesk chan structs.HelpDesk) {
	reads := make(chan structs.Job)
	readJob := structs.ReadOp{reads}
	machineChannel := make(chan structs.MachineInfo)
	r1 := rand.New(rand.NewSource(time.Now().UnixNano()))
	lazy := r1.Intn(2)
	menu.ArrayOfLaziness[number] = lazy
	string := ""
	for {

		read <- &readJob
		repeat := true
		select {
		case job := <-readJob.Resp:
			for repeat {
				getMachine <- &structs.GetMachine{job, machineChannel}
				machineInfo := <-machineChannel
				select {
				case machineInfo.Resp <- &structs.WriteMachine{job, reads}:
					job = <-reads
					if job.Result == 0 {
						HelpDesk <- structs.HelpDesk{machineInfo.Id, job.Operation}
						reads <- job
					} else {
						result := structs.WriteMagazine{job}
						write <- &result
						string = fmt.Sprintf("Worker number %d did job with result:  %f . Operation %c\n", number, job.Result, job.Operation)
						menu.ArrayOfSuccess[number]++
						repeat = false
					}
				case <-maybe(lazy == 0, time.After(variables.WorkerAwaits)):
					if variables.Issilent == false {
						fmt.Printf("Changing Machine, time is over, Worker %d isn't that lazy\n", number)
					}
				}
			}
		case <-time.After(variables.WorkerAwaits):
			string = ""
			fmt.Printf("Timed out on getting Job")
		}
		if variables.Issilent == false {
			fmt.Printf(string)
		}
		time.Sleep(variables.WorkerSleep)
	}

}

func maybe(b bool, c <-chan time.Time) <-chan time.Time {
	if !b {
		return nil
	}
	return c
}
