package client

import (
	"fmt"
	"structs"
	"time"
	"variables"
)

func Client(number int, read chan *structs.ReadMagazine) {
	anwser := make(chan structs.Job)
	anwserChannel := structs.ReadMagazine{anwser}
	string := ""
	for {
		read <- &anwserChannel
		select {
		case result := <-anwserChannel.Resp:
			string = fmt.Sprintf("Client number %d bought package with result: %f", number, result.Result)
		case <-time.After(variables.ClientAwaits):

		}
		if variables.Issilent == false {
			fmt.Printf("%s\n", string)
		}
		time.Sleep(variables.ClientSleep)
	}
}
