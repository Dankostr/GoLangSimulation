package menu

import (
	"fmt"
	"variables"
)

var ArrayOfLaziness = make([]int, variables.NoOfWorkers)
var ArrayOfSuccess = make([]int, variables.NoOfWorkers)

func Menu(jobListInfo chan chan int, magazineInfo chan chan int) {
	var action int
	infoChannel := make(chan int)
	for {
		fmt.Printf("Menu:\n1.Jobs to do\n2.Items in magazine\n3.How many workers\n4.How many clients\n5.Is worker lazy?\n6 How much jobs worker did?\n")
		fmt.Scanf("%d\n", &action)
		switch action {
		case 1:
			jobListInfo <- infoChannel
			fmt.Printf("Jobs to do: %d\n")
			<-infoChannel

			break
		case 2:
			magazineInfo <- infoChannel
			response := <-infoChannel
			fmt.Printf("Items in magazine: %d\n", response)
			break
		case 3:
			fmt.Printf("Number of workers: %d\n", variables.NoOfWorkers)
			break
		case 4:
			fmt.Printf("Nubmer of Clients: %d\n", variables.NoOfClients)
			break
		case 5:
			for i := 0; i < variables.NoOfWorkers; i++ {
				if ArrayOfLaziness[i] == 1 {
					fmt.Printf("The worker %d is lazy\n", i)
				} else {
					fmt.Printf("The worker %d is not lazy\n", i)

				}
			}

		case 6:
			for i := 0; i < variables.NoOfWorkers; i++ {
				fmt.Printf("The worker %d did %d jobs\n", i, ArrayOfSuccess[i])
			}

		}
	}
}
