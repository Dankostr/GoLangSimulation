package main

import (
	"boss"
	"client"
	"joblist"
	"machineroom"
	"magazine"
	"menu"
	"os"
	"service"
	"structs"
	"sync"
	"variables"
	"worker"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(variables.NoOfClients + variables.NoOfWorkers + 2)
	jobListInfo := make(chan chan int)
	magazineInfo := make(chan chan int)
	if len(os.Args) >= 2 {
		if os.Args[1] == "-s" {
			variables.Issilent = true
			wg.Add(1)
			go menu.Menu(jobListInfo, magazineInfo)
		}
	}
	readsJob := make(chan *structs.ReadOp)
	writesJob := make(chan *structs.WriteOp)
	readsMagazine := make(chan *structs.ReadMagazine)
	writeMagazine := make(chan *structs.WriteMagazine)
	getMachine := make(chan *structs.GetMachine)
	machineRepairChannel := make(chan structs.MachineRepair)
	serviceChannel := make(chan structs.HelpDesk)
	go machineroom.MachineRoom(getMachine, machineRepairChannel)
	go service.Service(serviceChannel, machineRepairChannel)
	go joblist.JobList(readsJob, writesJob, jobListInfo)
	go magazine.Magazine(readsMagazine, writeMagazine, magazineInfo)
	go boss.Boss(writesJob)
	for i := 0; i < variables.NoOfWorkers; i++ {
		go worker.Worker(i, readsJob, writeMagazine, getMachine, serviceChannel)
	}
	for i := 0; i < variables.NoOfClients; i++ {
		go client.Client(i, readsMagazine)
	}

	wg.Wait()
}
