package boss

import (
	"fmt"
	"math/rand"
	"structs"
	"time"
	"variables"
)

func Boss(write chan *structs.WriteOp) {
	for {
		r1 := rand.New(rand.NewSource(time.Now().UnixNano()))
		mathOperator := [2]byte{'+', '*'}
		job := structs.Job{r1.Float64() * 100, r1.Float64() * 100, mathOperator[r1.Intn(2)], 0}
		writeJob := structs.WriteOp{job, make(chan int)}
		write <- &writeJob
		if variables.Issilent == false {
			fmt.Printf("Boss created job: %f %c %f\n", job.X, job.Operation, job.Y)
		}
		time.Sleep(time.Duration(r1.Float64() * variables.BossSleep))
	}
}
