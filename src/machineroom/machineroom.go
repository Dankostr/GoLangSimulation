package machineroom

import (
	"machine"
	"structs"
	"variables"
)

func MachineRoom(read chan *structs.GetMachine, machineForRepair chan structs.MachineRepair) {
	sumMachines := make([]chan *structs.WriteMachine, 0)
	multiplyMachines := make([]chan *structs.WriteMachine, 0)
	sumMachinesRepairChannels := make([]chan chan int, 0)
	multiplyMachinesRepairChannels := make([]chan chan int, 0)
	for i := 0; i < variables.SumMachines; i++ {
		channel := make(chan *structs.WriteMachine)
		channelRepair := make(chan chan int)
		sumMachines = append(sumMachines, channel)
		sumMachinesRepairChannels = append(sumMachinesRepairChannels, channelRepair)
		go machine.Machine(channel, channelRepair)
	}
	for i := 0; i < variables.MultiplyMachines; i++ {
		channel := make(chan *structs.WriteMachine)
		channelRepair := make(chan chan int)
		multiplyMachines = append(multiplyMachines, channel)
		multiplyMachinesRepairChannels = append(multiplyMachinesRepairChannels, channelRepair)

		go machine.Machine(channel, channelRepair)
	}
	var SumMachinesIterator = 0
	var MultiplyMachinesIterator = 0
	for {
		select {
		case ask := <-read:
			if ask.Key.Operation == '+' {
				machine := sumMachines[SumMachinesIterator]
				ask.Resp <- structs.MachineInfo{SumMachinesIterator, machine}
				SumMachinesIterator++
				SumMachinesIterator %= variables.SumMachines
			} else {
				machine := multiplyMachines[MultiplyMachinesIterator]
				ask.Resp <- structs.MachineInfo{MultiplyMachinesIterator, machine}
				MultiplyMachinesIterator++
				MultiplyMachinesIterator %= variables.SumMachines
			}
		case repairment := <-machineForRepair:
			if repairment.Info.Type == '+' {
				repairment.Channel <- sumMachinesRepairChannels[repairment.Info.Id]
			} else if repairment.Info.Type == '*' {
				repairment.Channel <- multiplyMachinesRepairChannels[repairment.Info.Id]
			}
		}
	}

}
