package serviceworker

import (
	"structs"
	"time"
	"variables"
)

func ServiceWorker(ServiceInfo chan chan structs.ServiceInfo) {
	channelForMachine := make(chan int)
	channelGetTask := make(chan structs.ServiceInfo)
	for {
		ServiceInfo <- channelGetTask
		machineForService := <-channelGetTask
		time.Sleep(variables.TimeToArrive)
		machineForService.ChannelToMachine <- channelForMachine
		<-channelForMachine
		machineForService.ChannelToService <- machineForService.Info
		machineForService.ChannelToMachine <- channelForMachine

	}
}
