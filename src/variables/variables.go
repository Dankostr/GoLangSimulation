package variables

import "time"

var Issilent = false
var NoOfWorkers = 10
var NoOfClients = 3
var BossSleep = 100.0
var WorkerSleep = 1000 * time.Millisecond
var ClientSleep = 2000 * time.Millisecond
var WorkerAwaits = 1 * time.Second
var ClientAwaits = 1 * time.Second
var MagazineSize = 100
var JobListSize = 100
var MachineWorkTime = 1 * time.Second
var SumMachines = 1
var MultiplyMachines = 1
var MachineBrokenProbability = 0.8
var Fixers = 1
var TimeToArrive = 1 * time.Second
