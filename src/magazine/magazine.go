package magazine

import (
	"fmt"
	"structs"
	"variables"
)

func Magazine(read chan *structs.ReadMagazine, write chan *structs.WriteMagazine, info chan chan int) {
	magazine := make([]structs.Job, 0)
	for {
		select {
		case magazineread := <-read:
			if len(magazine) > 0 {
				magazineread.Resp <- magazine[0]
				magazine = magazine[1:]
			}
		case writeJob := <-maybe(len(magazine) < variables.MagazineSize, write):
			magazine = append(magazine, writeJob.Key)
			if variables.Issilent == false {
				fmt.Printf("Items in magazine%d\n", len(magazine))
			}
		case respond := <-info:
			respond <- len(magazine)
		}
	}
}

func maybe(b bool, c chan *structs.WriteMagazine) chan *structs.WriteMagazine {
	if !b {
		return nil
	}
	return c
}
