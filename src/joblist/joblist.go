package joblist

import (
	"fmt"
	"structs"
	"variables"
)

func JobList(read chan *structs.ReadOp, write chan *structs.WriteOp, info chan chan int) {
	jobs := make([]structs.Job, 0)
	for {
		select {
		case readJob := <-mayberead(len(jobs) > 0, read):
			if len(jobs) > 0 {
				readJob.Resp <- jobs[0]
				jobs = jobs[1:]
			}
		case writeJob := <-maybe(len(jobs) < variables.JobListSize, write):
			jobs = append(jobs, writeJob.Key)
		case respondchannel := <-info:
			for i := 0; i < len(jobs); i++ {
				fmt.Printf("Job:%f %c %f\n", jobs[i].X, jobs[i].Operation, jobs[i].Y)
			}
			respondchannel <- len(jobs)

		}
		if variables.Issilent == false {
			fmt.Printf("Size of Job List %d\n", len(jobs))
		}
	}

}

func mayberead(b bool, c chan *structs.ReadOp) chan *structs.ReadOp {
	if !b {
		return nil
	}
	return c
}

func maybe(b bool, c chan *structs.WriteOp) chan *structs.WriteOp {
	if !b {
		return nil
	}
	return c
}
