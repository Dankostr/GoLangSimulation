package machine

import (
	"math/rand"
	"structs"
	"time"
	"variables"
)

func Machine(read chan *structs.WriteMachine, repairChannel chan chan int) {
	var broken = false
	r1 := rand.New(rand.NewSource(time.Now().UnixNano()))
	for {
		select {
		case repairment := <-repairChannel:
			broken = false
			repairment <- 1
			<-repairChannel
		default:

			select {
			case readJob := <-read:
				job := readJob.Key
				if r1.Float64() > variables.MachineBrokenProbability {
					broken = true
				}
				if broken == true {
					job.Result = 0
				} else {
					job.DoJob()
				}

				time.Sleep(variables.MachineWorkTime)
				readJob.Resp <- job
				if broken == true {
					<-readJob.Resp
				}
			case repairment := <-repairChannel:
				broken = false
				repairment <- 1
				<-repairChannel
			}

		}
	}
}
