package structs

type ReadOp struct {
	Resp chan Job
}

type WriteOp struct {
	Key  Job
	Resp chan int
}
type ReadMagazine struct {
	Resp chan Job
}
type WriteMagazine struct {
	Key Job
}

type WriteMachine struct {
	Key  Job
	Resp chan Job
}

type MachineInfo struct {
	Id   int
	Resp chan *WriteMachine
}

type HelpDesk struct {
	Id   int
	Type byte
}

type MachineRepair struct {
	Info    HelpDesk
	Channel chan chan chan int
}

type ServiceTask struct {
	Info    HelpDesk
	Channel chan chan int
}

type ServiceInfo struct {
	Info             HelpDesk
	ChannelToMachine chan chan int
	ChannelToService chan HelpDesk
}

type GetMachine struct {
	Key  Job
	Resp chan MachineInfo
}

type Job struct {
	X         float64
	Y         float64
	Operation byte
	Result    float64
}

func (j *Job) DoJob() int {
	switch j.Operation {
	case '+':
		j.Result = j.X + j.Y
		return 1
	case '*':
		j.Result = j.X * j.Y
		return 1
	default:
		j.Result = 0
		return -1
	}
}
